import { setHeaders } from '~/utils/api';

export default async function ({ req }) {
  if (req && req.headers) {
    setHeaders(req.headers);
  }
  return true;
};
