import Vue from 'vue';
import { get } from '~/utils/api';
// import { setImaginaryEnv } from '~/plugins/imaginary';

export const state = () => ({
  locale: 'ru',
  settings: {
    breadcrumbs: []
  },
  env: null
});

export const mutations = {

  setSettings (state, payload) {
    state.settings = payload;
  },

  setEnv (state, payload) {
    state.env = payload;
    // setImaginaryEnv(payload);
  },

  setLocale (state, payload) {
    state.locale = payload;
  },

  setPageBreadcrumbs (state, payload) {
    // state.settings.breadcrumbs = payload
    Vue.set(state.settings, 'breadcrumbs', payload);
  }
};

// not use getters just for get state value!
// mapState or this.$store.state[.storeModuleName] instead

export const actions = {

  // load common data
  async nuxtServerInit ({ commit }) {
    const data = await get('initial-data');
    commit('setSettings', data.settings);
    commit('setEnv', data.env);
  }
};
