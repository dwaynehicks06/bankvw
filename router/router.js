import Vue from 'vue';
import Router from 'vue-router';
import scrollBehavior from '../.nuxt/router.scrollBehavior';

import '~/layouts/default.vue';

Vue.use(Router);

function interopDefault (promise) {
  return promise.then(m => m.default || m);
}

const Main = () => interopDefault(import('~/views/Main'));
const AboutBank = () => interopDefault(import('~/views/AboutBank'));
const ProductConditions = () => interopDefault(import('~/views/ProductConditions'));
const News = () => interopDefault(import('~/views/News'));
const Philosophy = () => interopDefault(import('~/views/Philosophy'));
const History = () => interopDefault(import('~/views/History'));
const Financialstatements = () => interopDefault(import('~/views/Financialstatements'));
const Officialinformation = () => interopDefault(import('~/views/Officialinformation'));
const Bankdetails = () => interopDefault(import('~/views/Bankdetails'));
const Forinvestors = () => interopDefault(import('~/views/Forinvestors'));
const Qualificationinfo = () => interopDefault(import('~/views/Qualificationinfo'));
const Internaldocuments = () => interopDefault(import('~/views/Internaldocuments'));
const Regulatoryinfo = () => interopDefault(import('~/views/Regulatoryinfo'));
const Productsdoc = () => interopDefault(import('~/views/Productsdoc'));

export function createRouter (ssrContext) {
  return new Router({
    mode: 'history',
    base: '/',
    linkActiveClass: 'nuxt-link-active',
    linkExactActiveClass: 'nuxt-link-exact-active',
    scrollBehavior,
    fallback: false,
    routes: [
      {
        path: '/',
        component: Main
      },
      {
        path: '/aboutbank',
        component: AboutBank
      },
      {
        path: '/productconditions',
        component: ProductConditions
      },
      {
        path: '/news',
        component: News
      },
      {
        path: '/philosophy',
        component: Philosophy
      },
      {
        path: '/history',
        component: History
      },
      {
        path: '/financialstatements',
        component: Financialstatements
      },
      {
        path: '/officialinformation',
        component: Officialinformation
      },
      {
        path: '/bankdetails',
        component: Bankdetails
      },
      {
        path: '/forinvestors',
        component: Forinvestors
      },
      {
        path: '/qualificationinfo',
        component: Qualificationinfo
      },
      {
        path: '/internaldocuments',
        component: Internaldocuments
      },
      {
        path: '/regulatoryinfo',
        component: Regulatoryinfo
      },
      {
        path: '/productsdoc',
        component: Productsdoc
      }
    ]
  });
};
