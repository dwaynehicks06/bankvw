/* eslint no-console: 0 */
/** @var process */

const name = 'VW Bank';
const API_PREFIX = 'fbr';
const HOST = process.env.FBR_HOST || process.env.HOST;

const IS_DEV = process.env.NODE_ENV === 'development';
const NUXT_HOST = '0.0.0.0';
const NUXT_PORT = process.env.NUXT_PORT || (IS_DEV ? 3000 : 80);
const API_PROXY_HOST = process.env.API_PROXY_HOST || 'http://nginx:80';
const API_SERVER_HOST = process.env.API_SERVER_HOST || API_PROXY_HOST; // (IS_DEV ? `http://localhost:${NUXT_PORT}` : `http://127.0.0.1:${NUXT_PORT}`)
const API_BROWSER_HOST = process.env.API_BROWSER_HOST || '/';
const API_BROWSER_PREFIX = process.env.API_BROWSER_HOST ? '' : 'api-proxy';
const IMG_HOST = process.env.IMG_HOST || API_PROXY_HOST;
const IMAGINARY_HOST = process.env.IMAGINARY_HOST || API_PROXY_HOST;
/* if (IS_DEV) { */console.log(`Name: ${name}, NUXT PORT: ${NUXT_PORT}, API PROXY HOST: ${API_PROXY_HOST}, IMAGINARY HOST: ${IMAGINARY_HOST}`);/* } */
const LINT = process.env.LINT || false;


export default {
  mode: 'universal',

  head: {
    htmlAttrs: {
      lang: 'ru',
      dir: 'ltr',
      itemscope: '',
      itemtype: 'http://schema.org/Store'
    },
    title: name,
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1, shrink-to-fit=no, viewport-fit=cover, user-scalable=no' },
      { hid: 'description', name: 'description', content: '' },
      // { property: 'vk:image', content: backUrlHost + '/img/sharing/fb-sharing.jpg' },
      { name: 'format-detection', content: 'telephone=no' },
      { name: 'format-detection', content: 'date=no' },
      { name: 'format-detection', content: 'address=no' },
      { name: 'format-detection', content: 'email=no' },
      { name: 'google', content: 'notranslate' },
      { 'http-equiv': 'x-ua-compatible', content: 'ie=edge' },
      { itemprop: 'name', content: 'VwBank' }
      // {itemprop: 'telephone', content: '88001005074',},
    ],
    link: [
      // { rel: 'preload', as: 'font', href: '/fonts/montserrat-500.woff2', crossOrigin: 'true'},
      { rel: 'preconnect', href: '//maps.gstatic.com' }
    ]
  },

  meta: {
    name,
    author: 'GGPA',
    description: '',
    theme_color: '#fff',
    lang: 'ru',
    ogTitle: '',
    ogDescription: '',
    ogHost: HOST,
    ogImage: `${HOST}/img/sharing/fb-sharing.jpg`,
    ogUrl: HOST
  },

  manifest: {
    lang: 'ru-RU',
    background_color: '#fff',
    theme_color: '#fff',
    name,
    description: '',
    short_name: name
  },

  router: {
    middleware: ['api-headers']
  },

  loading: { color: '#fff' },

  styleResources: {
    sass: [
      './assets/sass/dev.sass'
    ]
  },

  plugins: [
    { src: '~/plugins/imaginary.js' },
    { src: '~/plugins/vue-i18n.js' }
  ],

  buildModules: [
    '@nuxtjs/style-resources',
    '@nuxtjs/svg-sprite',
    '@nuxtjs/proxy',
    '@nuxtjs/pwa',
    '@nuxtjs/eslint-module'
  ],

  modules: [
    ['@nuxtjs/router', { path: 'router', DefaultRouter: true }]
  ],

  build: {
    extractCSS: true,

    extend (config, { isDev, isClient }) {
      if (isDev && isClient && LINT) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        });
      }
      config.module.rules.push(
        {
          test: /\.ya?ml$/,
          use: 'js-yaml-loader'
        }
      );
    },

    postcss: {
      plugins: {
        'postcss-initial': {},
        'postcss-momentum-scrolling': [
          'scroll',
          'auto'
        ],
        'postcss-object-fit-images': {},
        'postcss-focus': {},
        'autoprefixer': {
          grid: false,
          supports: false
        }
      }
    }
  },

  ...(IS_DEV ? { proxy: { '/api-proxy': { target: API_PROXY_HOST, pathRewrite: { '^/api-proxy': '/' }, ws: true } } } : {}),
  server: { port: NUXT_PORT, host: NUXT_HOST },
  env: {
    API_BROWSER_HOST,
    API_SERVER_HOST,
    HOST,
    IS_DEV,
    API_BROWSER_PREFIX,
    API_PREFIX
  }
};
