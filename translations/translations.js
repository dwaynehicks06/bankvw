import ruMessages from '~/translations/ru.yml';

export default {
  /*
  en: {
    ...enLocale,
    ...elementEnLocale
  },
  */
  ru: {
    ...ruMessages
  }
};
